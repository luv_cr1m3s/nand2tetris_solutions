# nand2tetris_solutions

* Solutions for projects from the first part of [NAND to tetris](https://www.coursera.org/learn/build-a-computer) course.

* First part includes chapter 1-6 from the [book](https://www.nand2tetris.org/book).

* Also was created [assembler](https://gitlab.com/luv_cr1m3s/HackAssm
) for cpu created in the course.
